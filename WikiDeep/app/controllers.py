import app.enums as enums
from app.models import Item, ItemData
from django.http import HttpRequest, HttpResponse, Http404
from django.shortcuts import render
from datetime import datetime

def create_page(sent_json):
    pass

def create_dir(sent_json):
    pass

def create(sent_json, path):
    if sent_json['name'] is None:
            raise Http404('Name is required')

    parent = Item.objects.filter(path=path)
    if len(parent) > 1: #TODO: make this a 500 error
        raise Http404('More than one path found - Error')
    elif len(parent) < 1:
        raise Http404('Parent path not found')

    new_path = path + "/" + sent_json['name']

    new_item = Item(name=sent_json['name'], type=int(sent_json['type']), parent_id=parent[0].id, path=new_path)
    new_item.save()

    if int(sent_json['type']) == enums.ItemType.file.value:
        new_item_text = ItemData(item_id=new_item.id, data=sent_json['text'])
        new_item_text.save()

    #TODO: add data to table

    return HttpResponse("Sucessful put")

def update(sent_json, path):
    if sent_json['name'] is None:
            raise Http404('Name is required')

    item = Item.objects.get(path=path)
    item_data = ItemData.objects.get(item_id=item.id)

    item_data.data = sent_json['text']

    item_data.save()

    return HttpResponse("Sucessful put")

def view(request, path):
    item = Item.objects.get(path=path.strip('/'))
    if item.type == enums.ItemType.directory.value:
        return render(
            request,
            'app/view_dir.html',
            {
                'title':'Directory Contents',
                'year':datetime.now().year,
            }
        )
    elif item.type == enums.ItemType.file.value:

        text = ItemData.objects.get(item_id=item.id)

        return render(
            request,
            'app/view_file.html',
            {
                'title':'Directory Contents',
                'year': datetime.now().year,
                'text': text.data,
            }
        )

def edit(request, path):
    item = Item.objects.get(path=path.strip('/'))
    if item.type == enums.ItemType.file.value:

        text = ItemData.objects.get(item_id=item.id)

        return render(
            request,
            'app/edit_file.html',
            {
                'title':'Directory Contents',
                'year': datetime.now().year,
                'text': text.data,
            }
        )