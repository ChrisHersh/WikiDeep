from enum import Enum

class ItemType(Enum):
    directory = 1
    file = 2

class FileType(Enum):
    html = 0
    markdeep = 1
    markdown = 2