"""
Definition of models.
"""

from django.db import models

# Create your models here.

# Arbitrary limits:
#   Name -> 50 chars
#   Path -> 500 chars

class Item(models.Model):
    name = models.CharField(max_length=100)
    type = models.IntegerField()

    parent_id = models.IntegerField()
    path = models.CharField(max_length=500)

class ItemData(models.Model):
    item = models.ForeignKey(Item, on_delete=models.CASCADE)
    data = models.TextField()
