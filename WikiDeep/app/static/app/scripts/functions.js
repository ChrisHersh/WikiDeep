﻿

// using jQuery
function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}
var csrftoken = getCookie('csrftoken');

function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}
$.ajaxSetup({
    beforeSend: function (xhr, settings) {
        if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
            xhr.setRequestHeader("X-CSRFToken", csrftoken);
        }
    }
});


function put_data() {
    var path = document.getElementById("path_input").value;
    var name = document.getElementById("name_input").value;
    var text = document.getElementById("text_input").value;
    var type = document.querySelector('input[name = "type"]:checked').value;

    var response = new Object();
    response.name = name;
    response.text = text;
    response.type = type;

    json_string = JSON.stringify(response);

    console.log("JSON : " + response);


    $.ajax({
        url: '/pages/' + path,
        type: 'PUT',
        data: json_string,
        contentType: "application/json; charset=utf-8",
        success: function(result) {
            console.log("Success " + result);
        },
        failure: function(result) {
            console.log("Failure " + result);
        }
    });
}

function post_data() {
    var path = window.location.pathname
    //var name = document.getElementById("name_input").value;
    var text = document.getElementById("text_input").value;

    var response = new Object();
    response.name = name;
    response.text = text;

    json_string = JSON.stringify(response);

    console.log("JSON : " + response);


    $.ajax({
        url: path,
        method: 'POST',
        data: json_string,
        contentType: "application/json; charset=utf-8",
        success: function (result) {
            console.log("Success " + result);
        },
        failure: function (result) {
            console.log("Failure " + result);
        }
    });
}
