﻿Vue.config.delimiters = ["[[", "]]"];

new Vue({
    el: '#editor',
    data: {
        input: '# hello'
    },
    methods: {
        format: function(str) {
            return window.markdeep.format(str);
        }
    }
})