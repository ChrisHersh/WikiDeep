"""
Definition of views.
"""

from django.shortcuts import render
from django.http import HttpRequest, HttpResponse, Http404
from django.template import RequestContext
from django.views import View
from datetime import datetime
from app.models import Item, ItemData
from django.views.decorators.csrf import csrf_exempt
import json
import app.controllers as controllers
import app.enums as enums



def create(request):
    return render(request, 
        'app/create.html', 
        {
            'title':'Home Page',
            'year':datetime.now().year,
        }
    )

class ItemView(View):

    def get(self, request, path, *args, **kwargs):

        mode = request.GET.get('mode', '')
        if mode != '':
            if mode == 'edit':
                return controllers.edit(request, path)
        return controllers.view(request, path)

    def post(self, request, path, *args, **kwargs):
        sent_json = json.loads(request.body.decode('utf-8'))
        return controllers.update(sent_json, path.strip('/'))

    def delete(self, request, path, *args, **kwargs):
        return HttpResponse("Sucessful delete")

    def put(self, request, path, *args, **kwargs):

        sent_json = json.loads(request.body.decode('utf-8'))
        return controllers.create(sent_json, path.strip('/'))
        
